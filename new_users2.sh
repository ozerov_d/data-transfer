#!/bin/bash

if [ -a getent.lock ]
then
  echo old process is still running, something wrong, please check
  exit
fi

touch /root/dataTransfer/users2.txt
NOW=$(date +"%Y%m%d-%H%M")
touch getent.lock
rm -rf pgroups.members
for i in `seq 10000 20000`; do getent group p${i} | awk -F ":$i:" '{print $2}' | tr ',' '\n' | grep -v '^$'; done > pgroups.members
for i in `cat pgroups.members | sort | uniq`
do 
  id=`id -u $i`
  grep $i /root/dataTransfer/users2.txt > /dev/null
  if [ ! $? = 0 ]
  then
    echo New User: $i ${id}
    echo ${NOW} ${i} >> /root/dataTransfer/users2.txt
  fi
done
rm -rf pgroups.members
rm -rf getent.lock


