#!/bin/bash

touch /root/dataTransfer/users.txt
NOW=$(date +"%Y%m%d-%H%M")
for i in `getent group svc-data_ra | awk -F ":" '{print $4}' | sed 's/,/ /g'`
do 
  id=`id -u $i`
  grep $i /root/dataTransfer/users.txt > /dev/null
  if [ ! $? = 0 ]
  then
    echo New User: $i ${id}
    echo ${NOW} ${i} >> /root/dataTransfer/users.txt
  fi
done

for u in `awk '{print $2}' /root/dataTransfer/users.txt`
do 
echo "\"/C=US/O=Globus Consortium/OU=Globus Connect Service/CN=41634e9c-95b8-11e4-ad90-22000a97197b/CN=${u}\" ${u}"
echo "\"/C=US/O=Globus Consortium/OU=Globus Connect Service/CN=8a72cd22-b895-11e6-9b54-22000a1e3b52/CN=${u}\" ${u}"
done > /root/dataTransfer/grid-mapfile

diff /etc/grid-security/grid-mapfile /root/dataTransfer/grid-mapfile > /dev/null
if ! [ $? = 0 ]
then
  echo "New grid-mapfile is generated"
  mv /etc/grid-security/grid-mapfile /etc/grid-security/grid-mapfile.${NOW}
  cp /root/dataTransfer/grid-mapfile /etc/grid-security/grid-mapfile
fi

rm -rf /root/dataTransfer/grid-mapfile

